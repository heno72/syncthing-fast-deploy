#!/bin/bash
# Fast deploy syncthing from zero

# Install required apps
sudo apt install git curl

# Add repository
sudo curl -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" \
    | sudo tee /etc/apt/sources.list.d/syncthing.list
printf "Package: *\nPin: origin apt.syncthing.net\nPin-Priority: 990\n" \
    | sudo tee /etc/apt/preferences.d/syncthing

# Install syncthing
sudo apt-get update
sudo apt-get install syncthing
sudo systemctl enable syncthing@${USER}.service
sudo systemctl start syncthing@${USER}.service

# Run syncthing in the background
# (it is already started so is not necessary
# syncthing 2>&1 > /dev/null &

# Wait for some time before opening browser
printf "[Installer]: Harap menunggu 5 menit\n"
sleep 300 && x-www-browser http://localhost:8384/
