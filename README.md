# Cara Install

Pertama-tama, buka terminal emulator, dan salin installer menggunakan git ke folder sementara
dengan mengetikkan ke terminal (salin perintah di bawah ini lalu paste di terminal):

```
git clone https://gitlab.com/heno72/syncthing-fast-deploy.git sfd
```

Tekan enter, lalu setelah selesai, jalankan program installer dengan mengetik ke terminal (salin perintah di bawah ini lalu paste di terminal):

```
sfd/bin/syncthing-fast-deploy.sh
```

Harap masukkan password jika diminta.

Jika browser sudah membuka halaman syncthing, maka mulailah melakukan setup:

![Halaman awal 'Syncthing'](img/0-homepage.png)

1. Bookmark dengan tombol `Ctrl+D`, dan simpan bookmark.

1. Tekan tombol 'Actions' di pojok kanan atas halaman, dan pilih 'Show ID'

   ![Lokasi tombol 'Actions'](img/1-action.png)

1. Copy ID yang ditampilkan dan kirimkan ID ke saya.

   ![Lokasi *Device Identification*](img/2-device-id.png)

1. Salin device ID berikut ini:
   ```
   UEXODBX-GLMXYSM-HIZBIPA-ZMEOYXL-VKQ4PHJ-L6G3TUS-NLJBNBG-TFDQBQJ
   ```

1. Dari halaman awal 'Syncthing', tekan tombol 'Add Remote Device' di pojok kanan bawah halaman.

   ![Tombol 'Add Remote Device'](img/3-add-remote.png)

1. Di kolom 'Device ID', paste device ID yang telah disalin di langkah sebelumnya.
   Kolom 'Device Name' diisi nama perangkat yang ditambahkan (misal 'Komputer Hendrik'), atau cukup dikosongkan.

   ![Kolom 'Device ID'](img/4-halaman-add-remote.png)

1. Tekan tombol 'Save'.

1. Di kolom sebelah kiri halaman awal syncthing, pilih 'Default Folder'

   ![Tampilan 'Default Folder'](img/5-default-folder.png)

1. Tekan tombol 'Edit' di 'Default Folder'. Halaman Edit Folder akan terbuka.

   ![Halaman 'Edit Folder ...'](img/6-edit-folder.png)

1. Centang perangkat yang hendak anda bagikan foldernya, lalu tekan 'save'.
